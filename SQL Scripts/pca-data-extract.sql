/*	PCA ENGLAND EXTRACT
	<DESCRIPTION>
	This code is used to extract the full table of PCA data from the NHSBSA Information Services Data Warehouse.
	Data is currently held in one table.
*/
SELECT
	*
FROM
	pca_data_ye